package zhuyouyong.bottomtab

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import androidx.appcompat.widget.AppCompatTextView

/**
 * @author zhuyouyong <zhuyouyong@hangsheng.com.cn>
 * Created on 2021/5/26.
 */
class LabelTextView(context: Context) : AppCompatTextView(context) {
    private var label: Bitmap? = null
    private var paddingStart = 0f
    private var paddingTop = 0f

    fun setLabel(id: Int, paddingStart: Float, paddingTop: Float) {
        label = BitmapFactory.decodeResource(resources, id)
        this.paddingStart = paddingStart
        this.paddingTop = paddingTop
    }

    override fun onDrawForeground(canvas: Canvas?) {
        super.onDrawForeground(canvas)
        label?.let {
            canvas?.drawBitmap(it, (width shr 1) + paddingStart, paddingTop, null)
        }
    }
}