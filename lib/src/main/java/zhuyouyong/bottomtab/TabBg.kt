package zhuyouyong.bottomtab

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.view.View
import androidx.core.content.ContextCompat

/**
 * @author zhuyouyong <zhuyouyong@hangsheng.com.cn>
 * Created on 2021/5/7.
 */
class TabBg(context: Context) : View(context) {
    private val path = Path()
    private val paint = Paint(Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG).apply {
        color = ContextCompat.getColor(context, R.color.zyy_bg_color)
    }
    private var progress = 0f
    private var step = 0f
    private var currentPos = 0f
    private val leftTop = PointF(0f, 0f)
    private val rightTop = PointF(0f, 0f)
    private val rightBottom = PointF(0f, 0f)
    private val leftBottom = PointF(0f, 0f)
    private val start = PointF(0f, 0f)
    private val joint = PointF(0f, 0f)
    private val end = PointF(0f, 0f)

    private var borderPath: Path? = null
    private lateinit var borderPaint: Paint

    fun setBorderColor(color: Int) {
        borderPath = Path()
        borderPaint = Paint(Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG).apply {
            this.color = color
            style = Paint.Style.STROKE
        }
    }

    fun onProgress(progress: Float) {
        this.progress = progress
        makePath()
        invalidate()
    }

    fun onEnd(pos: Int) {
        currentPos = pos * step * 2
        progress = 0f
    }

    private fun makePath() {
        start.x = currentPos + progress * step * 2
        joint.x = start.x + step
        end.x = joint.x + step

        path.apply {
            reset()
            moveTo(leftTop.x, leftTop.y)
            lineTo(start.x, start.y)
            lineTo(joint.x, joint.y)
            lineTo(end.x, end.y)
            lineTo(rightTop.x, rightTop.y)
            lineTo(rightBottom.x, rightBottom.y)
            lineTo(rightBottom.x, rightBottom.y)
            lineTo(leftBottom.x, leftBottom.y)
            lineTo(leftBottom.x, leftBottom.y)
            close()
        }
        borderPath?.apply {
            reset()
            moveTo(leftBottom.x, leftBottom.y)
            lineTo(leftTop.x, leftTop.y)
            lineTo(start.x, start.y)
            lineTo(joint.x, joint.y)
            lineTo(end.x, end.y)
            lineTo(rightTop.x, rightTop.y)
            lineTo(rightBottom.x, rightBottom.y)
        }
    }

    @SuppressLint("DrawAllocation")
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        rightTop.x = measuredWidth.toFloat()
        rightBottom.apply {
            x = rightTop.x
            y = measuredHeight.toFloat()
        }
        leftBottom.y = rightBottom.y
        joint.y = measuredHeight * 0.8f
        step = (measuredWidth shr 3).toFloat()
        paint.pathEffect = CornerPathEffect(step)
        if (::borderPaint.isInitialized) {
            borderPaint.pathEffect = paint.pathEffect
        }
        makePath()
    }

    override fun onDraw(canvas: Canvas) {
        canvas.drawPath(path, paint)
        borderPath?.let { canvas.drawPath(it, borderPaint) }
    }
}