package zhuyouyong.bottomtab

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import androidx.appcompat.widget.AppCompatImageView

/**
 * @author zhuyouyong <zhuyouyong@hangsheng.com.cn>
 * Created on 2021/5/29.
 */
class BorderImageView(context: Context) : AppCompatImageView(context) {
    private var paint: Paint? = null

    fun setBorderColor(color: Int) {
        paint = Paint(Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG).apply {
            style = Paint.Style.STROKE
            this.color = color
        }
    }

    override fun onDrawForeground(canvas: Canvas) {
        super.onDrawForeground(canvas)
        paint?.let {
            val center = (width shr 1).toFloat()
            canvas.drawCircle(center, center, center - 1, it)
        }
    }
}