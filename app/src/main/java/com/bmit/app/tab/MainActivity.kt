package com.bmit.app.tab

import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import zhuyouyong.bottomtab.CommonTab

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /*findViewById<BottomTab>(R.id.bottomTab).apply {
            setContent(
                Pair("首页", R.drawable.zyy_tab1),
                Pair("一键加油", R.drawable.zyy_tab2),
                Pair("车生活", R.drawable.zyy_tab3),
                Pair("我的", R.drawable.zyy_tab4)
            )
            setLabel(1, R.drawable.zyy_tab2_label)
            setBorderColor(Color.BLACK)
        }*/
        findViewById<CommonTab>(R.id.commonTabExt).apply {
            setContent(
                Pair("娱乐", R.drawable.app_tab_fun),
                Pair("导航", R.drawable.app_tab_nav),
//                Pair("智慧加油", R.drawable.app_tab_oil),
//                Pair("车生活", R.drawable.app_tab_vehicle),
                Pair("车生活", R.drawable.app_tab_about),
                Pair("我的", R.drawable.app_tab_about)
            )
            // setLabel(2, R.drawable.zyy_tab2_label)
            setBorderColor(Color.BLACK)
        }
    }
}