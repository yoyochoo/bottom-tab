# BottomTab

#### 介绍
A simple bottom tab navigation used by Android.

#### 使用方法
```kotlin
        findViewById<BottomTab>(R.id.bottomTab).apply {
            setContent(
                Pair("首页", R.drawable.zyy_tab1),
                Pair("一键加油", R.drawable.zyy_tab2),
                Pair("车生活", R.drawable.zyy_tab3),
                Pair("我的", R.drawable.zyy_tab4)
            )
            setLabel(1, R.drawable.zyy_tab2_label)
            setBorderColor(Color.BLACK)
        }
```